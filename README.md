# Architecture

## Cloud Services Utilized
The following are the cloud services that we have used to establish a centralized user management portal across cloud platforms (AWS, Azure). 
1. Entra ID
2. AWS Identity Center

## Architecture
I am leveraging Microsoft Entra ID as an Identity provider to contral user access to all my aws account and azure subscriptions. We have established an automatic sync between AWS Identity Center and Microsoft Entra ID. All the users and group created in the Entra ID will be reflected on to the AWS Identity center as well. Based on the permission sets that I configured will grant access to the resources based on the permissions. 

![idp_architecture](images/idp_architecture.jpeg)

## Steps to Implement the Syncronization
1. Starting with the AWS Identity Center, Set the Identity Provider as the external identity provider.

2. Now, download the metadata file from the aws and store it for future use

3. Navigate to Microsoft Entra AD and create a new enterprise application for AWS Identity Center

4. Onboard the users and groups based on requirement then move on to set up SAML Authentication

5. Under Single Sign On, choose SAML and upload the metadata file that we have downloaded from AWS. 

6. Based on the Azure Entra Plan, you should be able to sync user/groups or both. Along with sync of all the users or just the onboarded users. 

7. Similarly, Download the metadata file from Azure and Upload the same in AWS. 

8. First provisioning must be triggered manually. Once done, we should be able to see all the groups and users in the AWS account without provisioning any users in AWS. 

9. Now the groups and users can be assigned permission sets to specific accounts. 

Finally, based on the accesss provided to users they should be able to see the following,

![final_portal.png](images/final_portal.png)

